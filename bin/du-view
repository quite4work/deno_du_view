#!/usr/bin/env deno run --no-check --allow-read --allow-write --allow-env --allow-plugin --unstable

import * as du_view from "https://denopkg.com/quite4work/deno-du-view";
import { Command } from "https://deno.land/x/cmd@v1.2.0/mod.ts";
import { readLines } from "https://deno.land/std@0.90.0/io/bufio.ts";
import { Webview } from "https://deno.land/x/webview@0.5.6/mod.ts";

const cli = new Command();

cli.option(
  "-s --strip <number>",
  "Remove leading path elements",
  parseInt,
);
cli.command("generate [input_file]").option(
  "-o --out <path>",
  "Output file",
  "du.html",
).action(generate);
cli.command("show [input_file]").action(show);
cli.parse(Deno.args);

async function generate(input_file, { out }) {
  let { strip } = cli.opts();
  let input = await readInput(input_file);
  let html = du_view.sunburst(input, { strip });
  await Deno.writeTextFile(out, html);
}

async function show(input_file) {
  let { strip } = cli.opts();
  let input = await readInput(input_file);
  let html = du_view.sunburst(input, { strip });
  const webview = new Webview({
    url: `data:text/html,${encodeURIComponent(html)}`,
  });
  await webview.run();
}

async function readInput(input_file) {
  let input = "";
  if (input_file) {
    input = await Deno.readTextFile(input_file);
  } else {
    for await (const line of readLines(Deno.stdin)) {
      input += line + "\n";
    }
  }
  return input;
}
